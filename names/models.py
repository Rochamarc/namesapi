from django.db import models

class FirstName(models.Model):
    class Meta:
        db_table = 'first name'

    name = models.CharField(max_length=50)
    nationality = models.CharField(max_length=80)

    def __str__(self):
        return self.name 
    
class LastName(models.Model):
    class Meta:
        db_table = 'last name'
    
    name = models.CharField(max_length=50)
    nationality = models.CharField(max_length=80)

    def __str__(self):
        return self.name 