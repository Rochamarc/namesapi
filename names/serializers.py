from rest_framework import serializers

from .models import FirstName, LastName

class FirstNameSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('name', 'nationality')
        model = FirstName

class LastNameSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('name', 'nationality')
        model = LastName 
        