from rest_framework import generics 

from .models import FirstName, LastName
from .serializers import FirstNameSerializer, LastNameSerializer

from random import randint

class FirstNamesList(generics.ListCreateAPIView):
    queryset = FirstName.objects.all()
    serializer_class = FirstNameSerializer

class LastNamesList(generics.ListCreateAPIView):
    queryset = LastName.objects.all()
    serializer_class = LastNameSerializer

class RandomFirstName(generics.ListAPIView):
    def get_queryset(self):
        return FirstName.objects.all().filter(id = randint(1, len(FirstName.objects.all())))
    serializer_class = FirstNameSerializer

class RandomLastName(generics.ListAPIView):
    def get_queryset(self):
        return LastName.objects.all().filter(id = randint(1, len(LastName.objects.all())))
    serializer_class = LastNameSerializer