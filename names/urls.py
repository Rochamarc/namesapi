from django.urls import path 

from .views import *

urlpatterns = [
    path('first-names/', FirstNamesList.as_view()),
    path('last-names/', LastNamesList.as_view()),
    path('first-name/', RandomFirstName.as_view()),
    path('last-name/', RandomLastName.as_view()),
]